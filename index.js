const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mongoose = require("mongoose");
const cors = require('cors');
const User = require('./src/routes/user_routes')
require("dotenv").config();

var corsOption = {
  origin: "*",
};
app.use(cors(corsOption));

app.use(bodyParser.json()); //type json yg akan di terima
app.use('/v1', User)


mongoose
  .connect(process.env.mongo_docker, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(4000, console.log("koneksi berhasil"));
  })
  .catch((err) => console.log(err));
