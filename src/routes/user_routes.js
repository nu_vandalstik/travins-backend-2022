const express = require("express");
const router = express.Router();

var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const user = require('../controllers/user.controller')

router.post('/user', user.User)
router.get('/user', user.getAll)
router.get('/user/:userId', user.getUserById)
router.delete('/user/:userId', user.deleteCodeByID)
router.put('/user/:userId', user.updateUser)

module.exports = router