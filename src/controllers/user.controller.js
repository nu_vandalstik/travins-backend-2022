const User = require("../models/userModel");

exports.User = (req, res) => {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    mobile: req.body.mobile,
    birthdate: req.body.birthdate,
    address: req.body.address,
  });
  user.save((err, data) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }
    res.send({
      data: data,
    });
  });
};

exports.getAll = (req, res, next) => {
  const currentPage = req.query.page || 1;
  const perPage = req.query.perPage || 10;
  const Search = req.query.search;
  // console.log(Search)
  User.find()
    .countDocuments()
    .then((count) => {
      totalItems = count;
      return User.find({
        name: {
          $regex: Search,
          $options: "i",
        },
      })
        .skip((parseInt(currentPage) - 1) * parseInt(perPage))
        .limit(parseInt(perPage));
    })
    .then((result) => {
      res.status(200).json({
        message: "Data Blog Post berhasil di panggil",
        data: result,
        total_data: totalItems,
        per_page: parseInt(perPage),
        current_page: parseInt(currentPage),
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.getUserById = (req, res, next) => {
  const userId = req.params.userId;
  User.findById(userId)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          data: "Data Tidak DiTemukan!",
        });
      }

      res.status(200).json({
        message: "Data User Berhasil DiPanggil!",
        data: result,
      });
    })
    .catch((err) => next(err));
};

exports.deleteCodeByID = (req, res, next) => {
  const userId = req.params.userId;

  User.findById(userId)
    .then((user) => {
      if (!user) {
        res.status(404).send({
          message: "Data User Tidak DiTemukan!",
        });
      }
      return User.findByIdAndRemove(userId);
    })
    .then((result) => {
      res.status(200).json({
        message: "Data User Berhasil DiHapus!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateUser = (req, res, next) => {
  const userId = req.params.userId;

  User.findById(userId)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak DiTemukan!",
        });
      }
      (result.name = req.body.name),
        (result.email = req.body.email),
        (result.mobile = req.body.mobile),
        (result.birthdate = req.body.birthdate),
        (result.address = req.body.address);

      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Data Berhasil DiUbah!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};
